import React from 'react';
import Button from './Button';
import {Link} from 'react-router-dom';
import styles from "./Menu.css";

class Menu extends React.PureComponent {

    render() {
        return (
            <div className={styles.menu}>
                <Link to="/"><Button type="menu">Seznam uživatelů</Button></Link>
                <Link to="/add"><Button type="menu">Přidat uživatele</Button></Link>
            </div>
        )
    }
}

export default Menu;