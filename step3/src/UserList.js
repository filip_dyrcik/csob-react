import React from 'react';
import UserItem from './UserItem';

class UserList extends React.PureComponent {

    handleDelete = (id) => {
        this.props.onDelete(id)
    }

    render() {

        return (
            this.props.users.map(user =>
                <UserItem user={user} onDelete={this.handleDelete}/>
            )
        );
    }
}

export default UserList;