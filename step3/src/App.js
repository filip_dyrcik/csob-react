import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import './App.css';
import UserList from './UserList';
import Menu from './Menu';
import Form from './Form';

class App extends Component {

    constructor(props) {
        super(props);
		this.state = {
            users: [
                {
                    id: 1,
                    first_name: 'Filip',
                    last_name: 'Dyrčík',
                    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg'
                }, {
                    id: 2,
                    first_name: 'Lukáš',
                    last_name: 'Rubeš',
                    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg'
                }]
        }
    }

	NewForm = props => <Form maxId={Math.max(...this.state.users.map(user => user.id), 0)} onCreate={this.handleCreate} {...props} />;

	ListOfUsers = () => <UserList users={this.state.users} onDelete={this.handleDelete}/>;

	handleDelete = id => {
        const newUsers = this.state.users.filter((user) => user.id !== id)
        this.setState({
            users: newUsers,
        })
    };

	handleCreate = user => {
		this.setState({
			users: [...this.state.users, user]
		})
	};

	render() {
		return (
			<div className="App">
				<Menu/>
				<Route path="/" render={this.ListOfUsers} exact />
				<Route path="/add" render={this.NewForm}/>
			</div>
		);
	}
}

export default App;