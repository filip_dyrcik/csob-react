import React from 'react';
import classNames from 'classnames';
import styles from './Button.css';

const Button = props => {
    const {
        children,
        ...otherProps
    } = props;

    return (<button className={classNames(
        styles.btn, {
            [styles.btnAdd]: props.type === 'add',
            [styles.btnRemove]: props.type === 'remove',
            [styles.btnMenu]: props.type === 'menu'
        })} {...otherProps} >
        {children}
    </button>)};

export default Button;