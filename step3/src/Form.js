import React from 'react';
import Button from './Button';
import styles from './Form.css';

class Form extends React.Component {

    state = {
        first_name: '',
        last_name: ''
    };

    handleCreate = e => {
        e.preventDefault();

        const {
            maxId,
            onCreate,
            history,
        } = this.props;

        const user = {
            ...this.state,
            id: maxId + 1,
            avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg'
        };
        onCreate(user);
        history.push('/');
    };

    handleInputChange = e => {
        const data = {};
        data[e.target.name] = e.target.value;
        this.setState(data);
    };

    render() {
        return (
            <form className={styles.form} onSubmit={this.handleCreate}>
                <label className={styles.label} htmlFor="first_name">Jméno</label>
                <input type="text" name="first_name" onChange={this.handleInputChange}/>
                <label className={styles.label} htmlFor="last_name">Příjmení</label>
                <input type="text" name="last_name" onChange={this.handleInputChange}/>
                <Button type="add">Přidat</Button>
            </form>
        );
    }
}

export default Form;