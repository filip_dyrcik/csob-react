import React from 'react';
import Button from './Button';
import styles from './UserItem.css';

const UserItem = props => {

	const handleDelete = (id) => {
		props.onDelete(id)
	}

	return (
		<div className={styles.wrapper}>
			<img className={styles.avatar} src={props.user.avatar} alt="AVATAR" />
			<div className={styles.userDetails}>
				<div className={styles.name}>{props.user.first_name + ' ' + props.user.last_name}</div>
				<div className={styles.buttonBar}>
					<Button type="remove" onClick={() => handleDelete(props.user.id)}>Smazat</Button>
				</div>
			</div>
		</div>
	)
};

export default UserItem;