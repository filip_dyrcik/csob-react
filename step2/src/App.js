import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import './App.css';
import UserList from './UserList';
import Menu from './Menu';
import Form from './Form';

class App extends Component {
	render() {
		return (
			<div className="App">
				<Menu/>
				<Route path="/" component={UserList} exact />
				<Route path="/add" component={Form}/>
			</div>
		);
	}
}

export default App;
