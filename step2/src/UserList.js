import React from 'react';
import UserItem from './UserItem';

class UserList extends React.PureComponent {

    render() {
        const {users} = this.props;

        return (
            users.map(user =>
                <UserItem user={user}/>
            )
        );
    }
}

UserList.defaultProps = {
    users: [
        {
            id: 1,
            first_name: 'Filip',
            last_name: 'Dyrčík',
            avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg'
        }, {
            id: 2,
            first_name: 'Lukáš',
            last_name: 'Rubeš',
            avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg'
        }]
};

export default UserList;