import React from 'react';
import UserItem from './UserItem';

class UserList extends React.PureComponent {

	render() {
		const { users } = this.props;

		return (
			users.map(user =>
				<UserItem user={user}/>
			)
		);
	}
}

export default UserList;