import React from 'react';
import Button from './Button';
import styles from './Form.css';

class Form extends React.Component {

    render() {
        return (
            <form className={styles.form}>
                <label className={styles.label} htmlFor="first_name">Jméno</label>
                <input type="text" name="first_name"/>
                <label className={styles.label} htmlFor="last_name">Příjmení</label>
                <input type="text" name="last_name"/>
                <Button type="add">Přidat</Button>
            </form>
        );
    }
}

export default Form;