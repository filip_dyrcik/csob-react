import React, {Component} from 'react';
import './App.css';
import UserList from './UserList';
import Menu from './Menu';
import Form from './Form';

class App extends Component {
	render() {
		return (
			<div className="App">
				<Menu/>
				<UserList users={[
					{
						id: 1,
						first_name: 'Filip',
						last_name: 'Dyrčík',
						avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg'
					}, {
						id: 2,
						first_name: 'Lukáš',
						last_name: 'Rubeš',
						avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg'
					}]}/>
                <Form />
			</div>
		);
	}
}

export default App;
