import React from 'react';
import Button from './Button';
import styles from "./Menu.css";

class Menu extends React.PureComponent {

    render() {
        return (
            <div className={styles.menu}>
                <Button type="menu">Seznam uživatelů</Button>
                <Button type="menu">Přidat uživatele</Button>
            </div>
        )
    }
}

export default Menu;